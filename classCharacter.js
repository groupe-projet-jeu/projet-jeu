"use strict";

let gender = [0,1];
let listGenderDesignation = ["homme", "femme"];
let listPersonalPronoun = [["il", "le", "un"], ["elle", "la", "une"]];

let listCharacter = [];

let final = [];

let gardien = {
  "designation" : ["gardien", "gardienne"],
  "name" : [["Bob", "Robert", "Bobby"],["Babette", "Petunia", "Bérangère"]],
  "clue" : ["clef", "porte-Clef"],
  "behavior" : ["boiteux"],
  "goal" : ["veux de l'aide pour sortir les poubelles"]
};

let pamy = {
  "designation" : ["papy", "mamy"],
  "name" : [["Bob", "Robert", "Bobby"],["Babette", "Petunia", "Bérangère"]],
  "clue" : ["cheveux blancs", "canne"],
  "behavior" : ["lent", "pale", "tremblotant"],
  "goal" : ["veux raconter une histoire", "veux partager mes gateaux"]
};

listCharacter.push(gardien);
listCharacter.push(pamy);

class Character{
  constructor(job){
    this.resume = listCharacter[job];
    this.gender = gender[nbRandomList(gender)];
    this.designation = this.resume["designation"][this.gender];
    this.name = this.resume["name"][this.gender][nbRandomList(this.resume["name"][this.gender])];
    this.clue = this.resume["clue"][nbRandomList(this.resume["clue"])];
    this.behavior = this.resume["behavior"][nbRandomList(this.resume["behavior"])];
    this.location = "parc";
    this.goal = this.resume["goal"][nbRandomList(this.resume["goal"])];
  }
  sayWhoIAm(){
    return `Je suis ${this.name} ${listPersonalPronoun[this.gender][1]} ${this.designation} et je ${this.goal}.`;
  }
}

function createCharacter() {
  let i = nbRandomList(listCharacter);
  let job = listCharacter[i];
  let newNPC = new Character(i);
  final.push(newNPC);
  listCharacter.splice(i,1);
}

function nbRandomList(list) {
  let nbRandom = Math.floor((Math.random() * list.length))
  return nbRandom;
}

createCharacter();
createCharacter();
console.log(final[0].sayWhoIAm());
console.log(final[1].sayWhoIAm());